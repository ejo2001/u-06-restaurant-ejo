FROM python:3.9

WORKDIR /app

COPY ./requirements.txt /app

COPY ./src/* /app

COPY ./code /app

RUN pip install -r /app/requirements.txt

RUN apt update

RUN apt install sqlite3

RUN sqlite3 /app/data.sql -init /app/code

ENTRYPOINT ["python3", "/app/querys.py"]
