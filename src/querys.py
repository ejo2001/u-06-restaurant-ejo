'''
Resturangen
'''

import random
import sqlite3
import os
import sys
from fpdf import FPDF
from datetime import date
import smtplib, ssl
from dotenv import load_dotenv
from email.mime.application import MIMEApplication
from email.mime.text import MIMEText
from email.mime.multipart import MIMEMultipart
#from email.MIMEImage import MIMEImage

load_dotenv()

smtp_server = "smtp.gmail.com"
port = 587  # For starttls
sender_email = os.getenv('EMAIL')
password = os.getenv('PASSWORD')

pdf = FPDF()
pdf.add_page()
pdf.set_font("Arial", size = 15)


def clearscreen():
    stream = os.popen("clear")
    output = stream.read()
    print(output)

# Queries som variabler för att kunna hämta dem till funktioner


GET_ALL_DISHES = 'SELECT * FROM dishes;'

SEARCH_NUMBER = "SELECT id, name, price FROM dishes WHERE id = ?;"

SEARCH_TEXT = "SELECT id, name, price FROM dishes WHERE name LIKE '%{}%';"

# NEW='''SELECT id, name, price FROM dishes WHERE name LIKE ? OR id = ?;'''

FLAGS_S = '''SELECT id, name, price
from dish_flags
Join dishes on dishes.id = dish_flags.dish
WHERE flags LIKE '%{}%';'''

SHOW_ORDER = ''' SELECT name, price, ordername, quantity, notes
from order_dishes
JOIN dishes on dishes.id = order_dishes.dish
WHERE ordername = '{}';'''

SHOW_ORDER_ID = ''' SELECT id, name, quantity
from order_dishes
JOIN dishes on dishes.id = order_dishes.dish
WHERE ordername = '{}';'''

SQL2 = '''DELETE from order_dishes where ordername = ? AND dish = ?;'''

MENU_PROMPT = """----THE RESTAURANT----\n
Please make a choice!\n
1. Meny
2. Search and order dish by number
3. Search and order dish by text
4. Delete dish from order
5. Search from flag
6. Show Order
7. Exit
\n\nYour selection:"""


conn = sqlite3.connect('data.sql')
cur = conn.cursor()


def connect():
    return sqlite3.connect('data.sql')


ordernumber = random.randint(100000, 999999)


def tot_sum():
    s_order = SHOW_ORDER.format(ordernumber)
    data2 = cur.execute(s_order)
    summary = 0
    for row in data2:
        summary += int(row[1]) * int(row[3])
    return summary

def get_dishes():
    dishes = conn.execute(GET_ALL_DISHES).fetchall()
    all_dishes = []
    for dish in dishes:
        all_dishes.append(f"{dish[0]}.{dish[1]} {dish[3]}kr\n{dish[2]}\n")
    return all_dishes


def print_dishes():
    for dish in get_dishes():
        print(dish)

def check_int(x):
    try:
        val = int(x)
        return True
    except ValueError:
        return False



def menu():
    clearscreen()
    while (user_input := input(MENU_PROMPT)) != "7":
        clearscreen()
        if user_input == "1":
            clearscreen()
            print_dishes()
            input("Press Enter to continue...")
            clearscreen()
        elif user_input == "2":
            def search_by_number(conn):
                '''Sök via nummer genom placeholder query.
                dishes används för att stoppa in inputen i queryn'''
                with conn:
                    return conn.execute(search_by_number).fetchall()
            number = input("whats the number?")
            while check_int(number) == False:
                number = input("Error. Please enter a number: ")
            var = cur.execute(SEARCH_NUMBER, [number])
            for dish in var:
                order_input = input(f'''The dish is:\n{dish[0]}.{dish[1]} {dish[2]}kr

Would you like to order {dish[1]}? Y or N:''')

                if order_input == "Y" or order_input == "y":
                    quantity = input("How many would you like to order?")
                    to_chef = input("Notes to the chef:")
                    o_d_q = ordernumber, dish[0], to_chef, quantity
                    sql = '''INSERT OR REPLACE INTO order_dishes VALUES(?,?,?,?)'''
                    cur.execute(sql, o_d_q)
                    conn.commit()
                    print("Din beställning:")
                    s_order = SHOW_ORDER.format(ordernumber)
                    data2 = cur.execute(s_order)

                    for row in data2:
                        print(f"{row[2]}: {row[0]} {row[1]}kr {row[3]}st \n{row[4]}\n")
                    conn.commit()
                elif order_input == "N" or order_input == "n":
                    print("okidoki")
                else:
                    print("invalid input:")
                clearscreen()
        elif user_input == "3":
            def search_dish(conn):
                '''Sök via nummer genom placeholder query.
                dishes används för att stoppa in inputen i queryn'''
                with conn:
                    return conn.execute(search_dish).fetchall()

            text = input("Search dish with text?")
            dishes = SEARCH_TEXT.format(text)
            var = cur.execute(dishes)
            for dish in var:

                order_input_text = input(f'''The dish is:\n{dish[0]}.{dish[1]} {dish[2]}kr

Would you like to order {dish[1]}? Y or N:''')

                if order_input_text == "Y" or order_input_text == "y":
                    '''
                    Om man vill beställa så får skapas fler input som ingår i
                    variabel samt ordernummer för att sen skicka in i queryn.
                    Sen har jag valt att göra en ny query för vad som visas.
                    '''
                    quantity = input("How many would you like to order?")
                    to_chef = input("Notes to the chef:")
                    o_d_q = ordernumber, dish[0], to_chef, quantity
                    sql = '''INSERT INTO order_dishes VALUES(?,?,?,?)'''

                    cur.execute(sql, o_d_q)
                    conn.commit()
                    print("Din beställning:")

                    s_order = SHOW_ORDER.format(ordernumber)
                    data2 = cur.execute(s_order)

                    for row in data2:
                        print(f"{row[2]}: {row[0]} {row[1]}kr {row[3]}st \n{row[4]}\n")
                    conn.commit()
                elif order_input_text == "N" or order_input_text == "n":
                    print("okidoki")
                else:
                    print("invalid input:")
                clearscreen()

        elif user_input == "4":
            def delete_dish(conn):
                with conn:
                    return conn.execute(delete_dish)

            print("Din beställning:")

            s_order_id = SHOW_ORDER_ID.format(ordernumber)

            data2 = cur.execute(s_order_id)

            for row in data2:
                print(f"{row[0]}.{row[1]} {row[2]}st\n")
            conn.commit()

            delete_input = input("Vilken rätt vill du ta bort från beställningen:")
            delete_var = ordernumber, delete_input

            cur.execute(SQL2, delete_var)
            conn.commit()
            print("Din beställning:")

            s_order_id = SHOW_ORDER_ID.format(ordernumber)
            data2 = cur.execute(s_order_id)

            for row in data2:
                print(f"{row[0]}.{row[1]} {row[2]}st\n")
            conn.commit()
            clearscreen()

        elif user_input == "5":
            def search_flag(conn):
                with conn:
                    return conn.execute(search_flag).fetchall()

            text = input("Search flag?")
            dishes = FLAGS_S.format(text)
            var = cur.execute(dishes)

            for dish in var:
                print(f"\n{dish[0]}.{dish[1]} {dish[2]}kr")
            clearscreen()
        elif user_input == "6":
            s_order = SHOW_ORDER.format(ordernumber)
            data2 = cur.execute(s_order)
            print(f"\nYour ordernumber is: {ordernumber}\n___________________________\n")
            for row in data2:
                print(f"{row[0]} {row[1]}kr {row[3]}st \n{row[4]}\n")
            print(f"Total: {tot_sum()}")
            input("Press Enter to continue...")
            clearscreen()
        else:
            print("Invalid input try again!")
    clearscreen()





def mod_menu():
    while True:
        clearscreen()
        mod_choice = input("1. Visa maträtter\n\n2. Ta bort maträtt\n\n3. Lägg till maträtt\n\n4. Avsluta\n\n\n:")
        if mod_choice == "1":
            clearscreen()
            print_dishes()
            input("Press Enter to continue...")
        elif mod_choice == "2":
            clearscreen()
            print_dishes()
            which = input("Vilken maträtt vill du ta bort: ")
            cur.execute("DELETE FROM dishes WHERE id = ?", (which))
            conn.commit()
            input("\nDONE\n\nPress Enter to continue...")
        elif mod_choice == "3":
            clearscreen()
            print_dishes()
            food_name = input("Namn: ")
            food_price = input("Pris: ")
            food_desc = input("Deskription: ")
            cur.execute("SELECT * FROM dishes")
            new_id = (cur.fetchall()[-1][0]) + 1
            cur.execute("INSERT INTO dishes VALUES (?, ?, ?, ?)", (new_id, food_name, food_desc, int(food_price)))
            conn.commit()
            input("\nDONE\n\nPress Enter to continue...")
        elif mod_choice == "4":
            clearscreen()
            print("Ha en bra dag!")
            sys.exit()









def select_mode():
    iscustomer = input("1. Personal\n\n2. Kund\n\nÄr du personal eller kund?\n\n:")
    if iscustomer == "1":
        mod_menu()
    elif iscustomer == "2":
        menu()
    else:
        print("Error! Please enter valid input")
        input("Press Enter to continue...")
        select_mode()

def send_email(receiver_email):
    message = MIMEMultipart("alternative")
    message["Subject"] = "Kvitto från resturang"
    message["From"] = sender_email
    message["To"] = receiver_email
    message.attach(MIMEText("Tack för din bestälning! Ha en bra dag!","plain"))
    #message.attach(MIMEText(open("./Kvitto.pdf").read()))
    with open(f"./Kvitto_{ordernumber}.pdf", "rb") as f:
        attachment = MIMEApplication(f.read(),_subtype="pdf")
        attachment.add_header('Content-Disposition','attachment',filename=str("Kvitto.pdf"))
        message.attach(attachment)
    context = ssl.create_default_context()
    with smtplib.SMTP(smtp_server, port) as server:
        server.starttls(context=context)
        server.login(sender_email, password)
        server.sendmail(sender_email, receiver_email, message.as_string())

select_mode()
clearscreen()
if tot_sum() > 0:
    receiver_email = input("Skriv in din email: ")
    print("Tack för din beställning! Vi skickar kvitto till din email!")
    s_order = SHOW_ORDER.format(ordernumber)
    data2 = cur.execute(s_order)
    pdf.set_font("Arial", size = 40)
    pdf.cell(190, 10, txt = f"________RESTURANG________", ln = 2, align = 'C')
    pdf.set_font("Arial", size = 10)
    pdf.cell(20, 10, txt = f"Datum: {date.today()}", ln = 2, align = 'C')
    pdf.set_font("Arial", size = 30)
    pdf.cell(200, 30, txt = f"Bestälning:", ln = 2, align = 'C')
    pdf.set_font("Arial", size = 20)
    for row in data2:
        print(f"{row[0]} {row[1]}kr {row[3]}st \n{row[4]}\n")
        pdf.cell(200, 10, txt = f"{row[0]} {row[1]}kr {row[3]}st \n{row[4]}\n", ln = 2, align = 'C')
    pdf.set_font("Arial", size = 30)
    pdf.cell(50, 100, txt = f"Totalt: {tot_sum()}", ln = 2, align = 'C')
    pdf.output(f"Kvitto_{ordernumber}.pdf")
    send_email(receiver_email)
else:
    print("Tack för ditt besök, kom gärna tillbaka igen!")
input("Press Enter to continue...")
clearscreen()
conn.close()
