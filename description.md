RestaurangCLI

Beställ mat från meny, summera kostnad, få kvitto, möjlighet att ta bort slutsålda rätter 

Sprint 1

Gitlab
Sätt upp ett repo med pipeline.
Code quality - pep8, Lint, Flake8 
Unittest - skapa relevanta tester för endpoints i sprint 1
Requirements -fil med nödvändiga installationer
Dockerfil
Coveragefil- I syfte att se hur stor procent som är täckt av tester
Gitignore- för pycache, virtual environment m.m. 

DB i sqlite
Skapa seeds för databas med rätter(dishes) pris, moms, behövs även flaggor för vego, laktos m.m. 




Funktioner
Visa meny
Ska visa menyn med pris och namn. Flaggor.
Söka igenom meny (fritext och/eller flaggor i.e. vego, laktosfri, glutenfri, etc) 
Ska söka igenom menyn med fritext och flaggor.
Få ett beställningsnummer
Få beställningsnummer
Addera rätt till kundvagn (hålla koll på olika kundvagn) samt ändringar/notes (fritext) Med beställningsnummer.

Sprint 2

Funktioner
För personal att sätta ett rätt som “icke tillgänglig”
Funktion för att kunna ta bort icke tillgängliga rätter. 
sammanställning av beställning


Godkänn beställning
PDF kvitto (datum, lista på rätter, priser, moms, etc)
Ta bort rätt från beställningen

Sätt upp relevanta tester för endpoints i sprint 2

Implemtera koden till https://github.com/bczsalba/pytermgui

