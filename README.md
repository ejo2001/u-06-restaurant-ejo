# U06-restaurant-by-Ejo

# DISCLAIMER

This project is written in Swenglish

# Introduction

This project was initiated by Marcus Skold for a school project (Simply named U06). The project is about working 3 weeks on something of your own choice, then hand it over to the next person and let them improve/finish the project. In this case, I was that seccond person. 

This project is supposed to be a restaurant app (Think Uber or similair) that let's you order food from a restaurant, then get a receipt of your order. It is pure visualization only.

# Setup

This program is ment to be ran in docker. I will assume that you have docker installed on your computer (If not, follow this link https://docs.docker.com/get-docker/). 

## Setup the .env file

Set up a file named ".env"

```sudo touch .env```

Open the .env file with a texteditor of your choice (This example will use vim)

```sudo vim .env```

Once inside .env, write 

```
EMAIL=*Your email*
PASSWORD=*Your emails password*
```

If you do not wish to expose your email then feel free to create a new email on google, then enter those credentials instead (Make sure to allow less secure apps https://support.google.com/a/answer/6260879?hl=en)

## Launch the container

Now, to launch the program in docker, open a terminal and write

```sudo docker pull registry.gitlab.com/ejo2001/u-06-restaurant-ejo; sudo docker run -it --rm -p 587:587 --env-file *PATH/TO/YOUR/.env* registry.gitlab.com/ejo2001/u-06-restaurant-ejo```

It will download the docker image, then launch the container and start the program.


# Bugs/Thoughts

This program suffers from 3 things. 

1. It lacks proper SQL implementation. The database is there and it is somewhat implemented, but it has been implemented as an advanced array/dictionary rather than an actual database. While it does technically save previous orders, it is just a mess to work with.

2. Parts of the program has been written in English, and other parts in Swedish. It was like that when I got it, and I don't know which language the application is supposed to be in, so I let it be the way it is for now.

3. The code is MESSY. When I received the code, it was all just an enormous while loop. I have tried to improve it by splitting up some parts of the code into functions, but it is still a mess.

